package com.example.coursefriday.di

import com.example.coursefriday.network.CoursesService
import com.example.coursefriday.repository.CoursesRepository
import com.example.coursefriday.repository.CoursesRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class CoursesModule {

    companion object {
        const val BASE_URL = "https://run.mocky.io/"
    }

    @Provides
    @Singleton
    fun provideCourseBuilder(): CoursesService =
        Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
            .build().create(CoursesService::class.java)

    @Provides
    @Singleton
    fun provideCourseInterface(coursesService: CoursesService): CoursesRepository =
        CoursesRepositoryImpl(coursesService)
}

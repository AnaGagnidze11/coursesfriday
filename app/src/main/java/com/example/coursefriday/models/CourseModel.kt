package com.example.coursefriday.models

import com.google.gson.annotations.SerializedName

data class CourseModel(
    val topic: List<Topic>,
    val courses: List<Course>
)

data class Topic(
    val color: String,
    val duration: Int,
    val title: String,
    val type: String
)

data class Course(
    @SerializedName("background_color_precent")
    val backgroundColorPercent: String,
    val color: String,
    val image: String,
    @SerializedName("precent")
    val percent: String,
    val title: String
)
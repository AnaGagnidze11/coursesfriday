package com.example.coursefriday.repository

import com.example.coursefriday.models.CourseModel
import com.example.coursefriday.network.ResultControl

interface CoursesRepository {

    suspend fun getCoursesInfo(): ResultControl<CourseModel>
}
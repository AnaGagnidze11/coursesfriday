package com.example.coursefriday.repository

import com.example.coursefriday.models.CourseModel
import com.example.coursefriday.network.CoursesService
import com.example.coursefriday.network.ResultControl
import java.lang.Exception
import javax.inject.Inject

class CoursesRepositoryImpl @Inject constructor(private val coursesService: CoursesService): CoursesRepository {

    override suspend fun getCoursesInfo(): ResultControl<CourseModel> {
        return try {
            val result = coursesService.getCourses()
            if (result.isSuccessful){
                ResultControl.success(result.body()!!)
            }else{
                ResultControl.error(result.errorBody()!!.string())
            }
        }catch (e: Exception){
            ResultControl.error(e.message.toString())
        }
    }
}
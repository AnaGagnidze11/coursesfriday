package com.example.coursefriday.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.coursefriday.databinding.CourseItemLayoutBinding
import com.example.coursefriday.databinding.TopicsRecyclerLayoutBinding
import com.example.coursefriday.models.RecyclerItemModel
import com.example.coursefriday.models.Topic

class RecyclerViewAdapter(private val items: List<RecyclerItemModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val viewPool = RecyclerView.RecycledViewPool()
    private val topicRecyclerItems = mutableListOf<Topic>()

    companion object {
        private const val RECYCLER = 1
        private const val COURSES = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == RECYCLER)
            AnotherRecyclerViewHolder(
                TopicsRecyclerLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        else
            CoursesViewHolder(CourseItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is CoursesViewHolder){
            holder.bind()
        }else if (holder is AnotherRecyclerViewHolder){
            val childLayoutManager = LinearLayoutManager(holder.recyclerView.context, LinearLayoutManager.HORIZONTAL, false)
            holder.recyclerView.apply {
                layoutManager = childLayoutManager
                adapter = TopicsRecyclerViewAdapter(topicRecyclerItems)
                setRecycledViewPool(viewPool)
            }
        }
    }

    override fun getItemCount() = items.size

    inner class AnotherRecyclerViewHolder(private val binding: TopicsRecyclerLayoutBinding) :
        RecyclerView.ViewHolder(binding.root){
            val recyclerView: RecyclerView = binding.topicsRecyclerView
        }

    inner class CoursesViewHolder(private val binding: CourseItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root){
            private lateinit var courseItem: RecyclerItemModel
            fun bind(){
                courseItem = items[adapterPosition]
                if (courseItem.courses != null){
                    binding.txtBookingTime.text = courseItem.courses?.title
                }
            }
        }


    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return if (item.isRecycler == null) COURSES
        else RECYCLER
    }

    fun setTopicItems(topicItems: MutableList<Topic>){
        this.topicRecyclerItems.addAll(topicItems)
        notifyDataSetChanged()

    }
}
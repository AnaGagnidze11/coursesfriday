package com.example.coursefriday.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.coursefriday.databinding.TopicItemLayoutBinding
import com.example.coursefriday.models.Topic

class TopicsRecyclerViewAdapter(private val items: MutableList<Topic>): RecyclerView.Adapter<TopicsRecyclerViewAdapter.TopicViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicViewHolder = TopicViewHolder(
        TopicItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: TopicViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount()= items.size

    inner class TopicViewHolder(private val binding: TopicItemLayoutBinding): RecyclerView.ViewHolder(binding.root){
        private lateinit var item: Topic
        fun bind(){
            item = items[adapterPosition]
            binding.txtCourseName.text = item.title
            binding.txtAmountOfTime.text = "${item.duration.toString()} mins"
        }
    }

}
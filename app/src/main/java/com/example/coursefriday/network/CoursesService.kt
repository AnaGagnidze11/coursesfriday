package com.example.coursefriday.network

import com.example.coursefriday.models.CourseModel
import retrofit2.Response
import retrofit2.http.GET

interface CoursesService {

    @GET("v3/29db8caa-95cb-44be-aa3c-eee0aa406870")
    suspend fun getCourses(): Response<CourseModel>
}
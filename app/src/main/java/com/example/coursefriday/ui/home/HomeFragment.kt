package com.example.coursefriday.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.coursefriday.R
import com.example.coursefriday.adapters.RecyclerViewAdapter
import com.example.coursefriday.databinding.FragmentHomeBinding
import com.example.coursefriday.models.RecyclerItemModel
import com.example.coursefriday.models.Topic
import com.example.coursefriday.network.ResultControl

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: RecyclerViewAdapter
    private val viewModel: HomeViewModel by viewModels()

    private val topicItems = mutableListOf<Topic>()
    private val recyclerItems = mutableListOf<RecyclerItemModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (_binding == null)
            _binding = FragmentHomeBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init(){
        observe()
        adapter = RecyclerViewAdapter(recyclerItems)
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
    }

    private fun observe(){
        viewModel.courses.observe(viewLifecycleOwner, {
            when(it.status){
                ResultControl.Status.SUCCESS -> {
                    adapter.setTopicItems(it.data?.topic!!.toMutableList())
                    recyclerItems.add(RecyclerItemModel(true, null))
                    it.data.courses.forEach {
                        recyclerItems.add(RecyclerItemModel(null, it))
                    }
                }
                ResultControl.Status.ERROR -> {
                    Toast.makeText(requireContext(), "Not working", Toast.LENGTH_SHORT).show()
                }
                ResultControl.Status.LOADING -> {}
            }
        })
    }





    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
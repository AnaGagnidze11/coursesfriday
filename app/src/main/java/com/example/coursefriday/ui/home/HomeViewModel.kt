package com.example.coursefriday.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.coursefriday.models.CourseModel
import com.example.coursefriday.network.ResultControl
import com.example.coursefriday.repository.CoursesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val coursesRepository: CoursesRepository): ViewModel() {

    private val courses_ = MutableLiveData<ResultControl<CourseModel>>()
    val courses: LiveData<ResultControl<CourseModel>> = courses_



    fun getCoursesInfo(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val courseInfo = coursesRepository.getCoursesInfo()
                courses_.postValue(courseInfo)
            }
        }
    }
}